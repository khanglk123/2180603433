| Title: | User Feedback Feature |
| :-------- | :----------------- |
| Value Statement: | As a customer, <br>I would like to be able to give feedback on the product, <br> my experience or submit store advice, to share your opinion and help improve the store's services.  |
| Acceptance Criteria: | Scenario 1:  <br>Given I am on the home page or in a dedicated section for feedback. <br>When I click on a "Feedback" or "Provide Feedback" button or link. <br>Then I am filling out the feedback form. <br> <br> <br>Scenario 2: <br>Given I have submitted feedback through the application.<br>When I want to check the status of my feedback.<br>Then there should be a "Feedback History" or "My Feedback" section in the application where I can view the status and responses to my feedback. <br> |
| Definition of Done | <ul> <li>Unit Tests Passed</li> <li>Acceptance Criteria Met</li> <li>Code Reviewed</li> <li>Functional Tests Passed</li> <li>Non-Functional Requirements Met</li> <li>Product Owner Accepts User Story</li> </ul> |
| Owner: | Khang | Owner |
| Iteration: | Unscheduled |
| Estimate: | 5 Points |

![](Figma_basics.png) 
![](395311693_1349841748959287_2337063645454034943_n.png)


|      Title          |   Show menu of books                                         ||
|----------------|-------------------------------|-----------------------------|
|Value Statement|        As a bookseller, I want to be able to view a menu that lists all the available books to the customer, so that they can easily browse and select the book they want to buy. |          |
|Acceptance Criteria          |Scenario 1 : <br>Given the menu display a list of books, each book should be displayed with its title, author, and a brief description,When the customers open app or click on the list of books, Then a list with many book genres appears, this menu should be easy to navigate and scroll through and Clicking on a book should open a detailed view or page providing more information and option as adding it to wishlist or purchasing it              |      |     
|Definition of Done         |Unit Tests Passed <br> Acceptance Criteria Met <br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story||
|		Owner			|			Tran Thanh Tuong					|			|				
|			Iteration		|		Unscheduled							|			|			
|					Estimate|5 Points								||

![giaodienus](/uploads/f92acdccc3715e5441be2d44f777d03d/giaodienus.png)
![giaodientc](/uploads/08f3ddbad4c0049d1c01b93c2f17360b/giaodientc.png)




# 2180608413
|       Title    		| Search books |
|------------------------|-------------------------------|
|Value Statement	|As a customer, I want to find books quickly so I can view or buy the books I want            |
|Acceptance Criteria        |Given that  to already know the information of book.<br>When customers enter into the search box.<br> Then a list of related books appears for customers to search.|
| Definition of Done| 1. performs validation and error handling for inputs and outputs.<br>2.meets the performance, security, usability, and reliability requirements.<br>3. passes all the unit tests, integration tests, and acceptance tests.
|Owner          |Hoai Phuong| |
 | Interation: | Unscheduled 
 | Estimate: | 5 point

 ![Search_books](/uploads/ee42018bdb28f35a0d256904f9786ba3/Search_books.png)

#2180608573

| Title: | Manage Books |
| ------ | ------ |
| Value Statement: | As a book manager, I want to update, delete and search books in the database, so that I can manage the inventory and availability of books. |
| Acceptance Criteria: | <ul> <li>The book manager can access a web page with a form to enter the book details (title, author, genre, ISBN, etc.)</li> <li>The book manager can submit the form and see a confirmation message that the book has been added to the database</li> <li>The book manager can view a list of all the books in the database with their details</li> <li>The book manager can edit or delete any book from the list by clicking on a button</li> <li>The book manager can search for a book by entering a keyword or a filter (e.g. genre, author, etc.)</li> </ul> |
| Definition of Done | <ul> <li>Acceptance Criteria Met</li> <li>Code reviewed</li> <li>Product owner Accepts User story</li> </ul> |
| Owner | Book Manager |
| Iteration | Unscheduled |
| Estimate | 5 Points |

# Main User Interface
![mainUS](/uploads/ffc86b329d6e938ef5f012a33614a843/mainUS.png)

# Menu Items – Specific Books
![menuUS](/uploads/1a812a0b3f9407d817439baeaf381632/menuUS.png)

![comboBoxUS](/uploads/542d510f91dc5668e19d4f10887d4e18/comboBoxUS.png)

# Form Edit Book
![editUS](/uploads/4ecc2bb8a811d8134a02d28f59bd7657/editUS.png)

# Form Book Detail
![detailUS](/uploads/15ac7ea3fee6de562ffb811103687dbe/detailUS.png)

# Form Import Books
![importUS](/uploads/821a94fdc00c534bb2aed96d1cdf29e1/importUS.png)



