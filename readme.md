
| Title: | FeedBack |
| :-------- | :----------------- |
| Value Statement: | As a customer, <br>I would like to be able to give feedback on the product, <br> my experience or submit store advice, to share your opinion and help improve the store's services.  |
| Acceptance Criteria: | Acceptance Criterion 1:  <br>Customers need to be notified of successful submission of their feedback or advice <br>All feedback must be stored in the store's database for future reference and analysis <br>If necessary, the store should have the ability to contact customers to request additional information or provide personal feedback <br> <br> <br>Acceptance Criterion 2: <br>If any changes or improvements are made to products or services based on customer feedback, the store should notify customers about this. <br>The store should have a clear policy on accepting or rejecting inappropriate or rule-violating feedback <br>The system needs to undergo performance testing to ensure that the process of providing feedback does not negatively impact the user experience <br> |
| Definition of Done: |  - Unit test passed <br> - Acceptance criteria are met <br> - The code has been evaluated <br> - Functional test passed <br> - Non-functional requirements have been met <br> - Save and rate tests |
| Owner: | Khang | Owner |
| Iteration: | Unscheduled |
| Estimate: | 5 Points |

![](Figma_basics.png) 









|       Title    		| Search books |
|------------------------|-------------------------------|
|Value Statement	|As a customer, I want the store to be able to quickly find books that can meet my needs            |
|Acceptance Criteria        |1.Identify the user:  The first step is to identify the user who will be using the book manager..<br>2.Define the goal: The next step is to define the goal of the user..<br>3.Break down the story into tasks: Once you have written the user story, you can break it down into smaller tasks that need to be completed in order to achieve the goal. include designing a search interface, implementing search functionality, and testing the search feature.<br> 4.Prioritize tasks: Finally, you can prioritize the tasks based on their importance and difficulty|
|Owner          |customers| |
 | Interation | 5 point|
 








|      Title          |   Show menu of books                                         ||
|----------------|-------------------------------|-----------------------------|
|Value Statement|        As a bookseller, I want to be able to view a menu that lists all the available books to the customer, so that they can easily browse and select the book they want to buy. |          |
|Acceptance Criteria          |Given the menu display a list of books, each book should be displayed with its title, author, and a brief description.<br>When the customers open app or click on the list of books. <br> Then a list with many book genres appears, this menu should be easy to navigate and scroll through and Clicking on a book should open a detailed view or page providing more information and option as adding it to wishlist or purchasing it              |      |     
|Definition of Done         |Unit Tests Passed <br> Acceptance Criteria Met <br>Code Reviewed<br>Functional Tests Passed<br>Non-Functional Requirements Met<br>Product Owner Accepts User Story||
|		Owner			|			Tran Thanh Tuong					|			|				
|			Iteration		|		Unscheduled							|			|			
|					Estimate|5 Points								||

![giaodien](https://gitlab.com/2180608203/2180608203/uploads/ed29aa7b315d76d44ba8ca3fef908627/giaodien.png)

| Title: | Manage Books |
| ------ | ------ |
| Value Statement: | As a book manager, I want to update, delete and search books in the database, so that I can manage the inventory and availability of books. |
| Acceptance Criteria: | <ul> <li>The book manager can access a web page with a form to enter the book details (title, author, genre, ISBN, etc.)</li> <li>The book manager can submit the form and see a confirmation message that the book has been added to the database</li> <li>The book manager can view a list of all the books in the database with their details</li> <li>The book manager can edit or delete any book from the list by clicking on a button</li> <li>The book manager can search for a book by entering a keyword or a filter (e.g. genre, author, etc.)</li> </ul> |
| Definition of Done | <ul> <li>Acceptance Criteria Met</li> <li>Code reviewed</li> <li>Product owner Accepts User story</li> </ul> |
| Owner | Book Manager |
| Iteration | Unscheduled |
| Estimate | 5 Points |



